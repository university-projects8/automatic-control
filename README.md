# Automatic Control

## Part 1

In this part we will study the dynamical model of a violin string and bow considering that the bow is dragged on the string with a constant velocity. The mechanical model is comprised of a mass M and a spring k lying on a belt moving with a constant velocity b. The stiction between the mass and the belt is modelled as:

<img src="https://latex.codecogs.com/gif.latex?F_b(\dot{x})  = \left \{ \begin{array} {ll}\left(\left(\dot{x}-b \right )-c \right )^2+d & for \; \dot{x}\geq b\\ -\left(\left(\dot{x}-b \right )-c \right )^2-d& otherwise \\ \end{array} \right.  " />


A) Provide the state space representation of the system using phase variables, find its equilibria and investigate its stability around each equilibrium using the indirect Lyapunov Method.

B) Using M=3, k=3, c=2, d=3: Find the equilibrium and investigate the system's stability for b=1, b=2, b=2.1 based on the phase portrait of the system.
