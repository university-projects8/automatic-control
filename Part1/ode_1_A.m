function [dx] = ode_violin(x,b)

     global M; global k; global c; global d;
     if (x(2)>=b)
               F_b1=(x(2)-b-c)^2+d;
     else
               F_b1=-(x(2)-b+c)^2-d;
     end
     dx(1)=x(2);
     dx(2)=(-F_b1-k*x(1))/M;
     dx=dx';
     if isnan(dx)
          return;
     end
end

