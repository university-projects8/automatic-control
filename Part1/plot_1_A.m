function[] = plot_1_A(config,i,t,y)
%Trajectory and time responses
global run; 
global init; global b; global equilibrium;
fname=sprintf('PP_%d_%d.png',config,run);
global fpath;
figure('Name','Trajectory')
plot(y(:,1),y(:,2))
hold on
plot(equilibrium(1),equilibrium(2),'r*')
line(get(gca,'XLim'), [0 0],'Color','k', 'LineStyle', '--');
line([0 0], get(gca,'YLim'), 'Color','k', 'LineStyle', '--');
title({'Trajectory',sprintf('Initial Conditions = [ %.2f , %.2f ]', ...
          init(1,config), init(2,config))})
legend(sprintf('b=%.2f',b(i)));

saveas(gca, fullfile(fpath, fname), 'png');

%Time_Response
fname=sprintf('TR_%d_%d.png',config,run);
figure('Name','Time Response')
subplot(2,1,1)
plot(t,y(:,1))
title({'Time Response of x(1)',sprintf('Initial Condition = %.2f', init(1,config))})

subplot(2,1,2)
plot(t,y(:,2))
title({'Time Response of x(2)',sprintf('Initial Condition = %.2f', init(2,config))})

saveas(gca, fullfile(fpath, fname), 'png');
