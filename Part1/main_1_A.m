%% Main File

%Initializing parameters
global M; global k;global c; global d; global run; run=1;
M=3;  k=3; c=2;  d=3; global equilibrium;
global b; b=[1,2,2.1];  
global fpath; %Where the files will be saved.
fpath='C:\Users\ifout\Desktop\plots';
%Inital Conditions defined by column
global init; init=[ 1,-1,2,-5 ;...
                    1,-1,5,-2]; %Initial Conditions for Phase Portraits
%Calling ode_1_A and save results 

t=[];x1=[];x2=[];
for i=1:3
     %NOTE: in order for the plots to appear nice, it would be advisable, not to
     %run the simulation in a loop, but with each value of b separately.
     %NOTE: for plot saving, the run variable initializes each time the script is run.
     equilibrium=[((b(i)-c)^2+d)/k;0];
     config=1;
     [t,y] = ode45(@(t,y) ode_1_A(y,b(i)),[0 200],init(:,config));
     t1=t;
     x1=[y(:,1),y(:,2)];
     plot_1_A(config,i,t1,x1) %t and y given as column vectors
     run=run+1;
     config=2;
     [t,y] = ode45(@(t,y) ode_1_A(y,b(i)),[0 200],init(:,config));
     t2=t;
     x2=[y(:,1),y(:,2)];
     plot_1_A(config,i,t2,x2) %t and y given as column vectors
     run=run+1;         
     config=3;
     [t,y] = ode45(@(t,y) ode_1_A(y,b(i)),[0 200],init(:,config));
     t3=t;
     x3=[y(:,1),y(:,2)];
     plot_1_A(config,i,t3,x3) %t and y given as column vectors
     run=run+1; 
     config=4;
     [t,y] = ode45(@(t,y) ode_1_A(y,b(i)),[0 200],init(:,config));
     t4=t;
     x4=[y(:,1),y(:,2)];
     plot_1_A(config,i,t4,x4) %t and y given as column vectors
     run=run+1;    
     %Phase Portrait
     %Create a total timespan for all simulations
     tspan=sort([t1;t2;t3;t4]);
     delete=[];
     for k=1:(length(tspan)-1)
          if tspan(k)==tspan(k+1)
               delete=[delete k+1];
          end
     end
     tspan(delete)=[];
     %Interpolate data in the new timespan
     x11=interp1(t1,x1(:,1),tspan,'spline');
     x12=interp1(t2,x2(:,1),tspan,'spline');
     x13=interp1(t3,x3(:,1),tspan,'spline');
     x14=interp1(t4,x4(:,1),tspan,'spline');
     x21=interp1(t1,x1(:,2),tspan,'spline');
     x22=interp1(t2,x2(:,2),tspan,'spline');
     x23=interp1(t3,x3(:,2),tspan,'spline');
     x24=interp1(t4,x4(:,2),tspan,'spline');
     %Finally print tha phase portrait
     fname=sprintf('PP_%d.png',i);
     figure('Name','Phase Portrait')
     plot(x11,x21)
     hold on
     plot(x12,x22)
     plot(x13,x23)
     plot(x14,x24)
     line(get(gca,'XLim'), [0 0],'Color','k', 'LineStyle', '--');
     line([0 0], get(gca,'YLim'), 'Color','k', 'LineStyle', '--');
     plot(equilibrium(1),equilibrium(2),'r*')
     title({'Phase Portrait',sprintf('b=%.2f',b(i))})
     legend(sprintf('x1_0=%d, x2_0=%d',init(1,1),init(2,1)), ...
            sprintf('x1_0=%d, x2_0=%d',init(1,2),init(2,2)),...
            sprintf('x1_0=%d, x2_0=%d',init(1,3),init(2,3)),...
            sprintf('x1_0=%d, x2_0=%d',init(1,4),init(2,4)));
     saveas(gca, fullfile(fpath, fname), 'png');
     hold off
     close all;
end
