
 global c;
 c=1;

global m; global r; global k; global I; global J; global mi; global g;
global m_hat; global r_hat; global k_hat; global I_hat; global J_hat; global mi_hat;
I=5.6;m=20;r=0.25;k=1900;J=6.18;mi=1; g=9.8;
m_hat=20; k_hat=2000;J_hat=6;I_hat=5;r_hat=0.25;mi_hat=1.25;

global w1; global w2; global w3; global w4; global w5; global w6; global w7; global w8;

m_min=19; m_max=24; 
k_min=1500;k_max=2500;
J_min=5; J_max=10; 
I_min=4; I_max=10;
r_min=0.2; r_max=0.5;
mi_min=0.5; mi_max=3; 

%% Max Values of Uncertainty Terms.
mrj_k_max=m_max*r_max*J_max/k_min;
mrj_k_hat=m_hat*r_hat*J_hat/k_hat;
mrj_k_min=m_min*r_min*J_min/k_max;
mrj_i_max=m_max*r_max*J_max/I_min;
mrj_i_hat=m_hat*r_hat*J_hat/I_hat;
mrj_i_min=m_min*r_min*J_min/I_max;
m2r2j_ik_max=m_max^2*r_max^2*J_max/(k_min*I_min);
m2r2j_ik_hat=m_hat^2*r_hat^2*J_hat/(k_hat*I_hat);
m2r2j_ik_min=m_min^2*r_min^2*J_min/(k_max*I_max);
kj_i_max=k_max*J_max/I_min;
kj_i_hat=k_hat*J_hat/I_hat;
kj_i_min=k_min*J_min/I_max;
ij_k_max=I_max*J_max/k_min;
ij_k_hat=I_hat*J_hat/k_hat;
ij_k_min=I_min*J_min/k_max;
w1=max([mrj_k_hat-mrj_k_min,mrj_k_max-mrj_k_hat]);
w2=max([mrj_i_hat-mrj_i_min,mrj_i_max-mrj_i_hat]);
w3=max([m2r2j_ik_hat-m2r2j_ik_min,m2r2j_ik_max-m2r2j_ik_hat]);
w4=max([kj_i_hat-kj_i_min,kj_i_max-kj_i_hat]);
w5=max([k_hat-k_min,k_max-k_hat]);
w6=max([mi_hat-mi_min,mi_max-mi_hat]);
w7=max([ij_k_hat-ij_k_min,ij_k_max-ij_k_hat]);
w8=max([J_hat-J_min,J_max-J_hat]);

init=[0;0;1;1];
global Control; global time;

global l;      %l1=[5,8,10,15,20,30]; %l2=[11,12,13];
global epsilon; %epsilon1=[1e-5,0.001,0.1,1,10];%epsilon2=[5e-1,,1,2];

%    The following script contains the code for 1st level sensitivity analysis
%    Sensitivity1
%    The following script contains the code for 2nd level sensitivity analysis
%    Sensitivity2

     Control=[];time=-1;
     epsilon=1; l=11;
     
     options = odeset('RelTol',1e-3,'AbsTol',1e-3);
     [t1,y1] = ode45(@(t,y) ode_c(t,y),[0 5],init ,options);
          %Second Version with u_max
          %[t1,y1] = ode23s(@(t,y) ode_2_C_sat(t,y,120),[0 20],init ,options);

     %t1=t1(200:end);y1=y1(200:end);
     fpath='C:\Users\ifout\Desktop\plots\c';
     for i=1:4
          fname=sprintf('2A_State%d.png',i);
          figure('Name','State')
          plot(t,y(:,i))
          title({sprintf('State%d',i),sprintf(...
               'Initial Conditions = [ 0 , 0, %.2f , %.2f ]',init(3),init(4))})

          saveas(gca, fullfile(fpath, fname), 'png');
          fname=sprintf('2A_Error%d.png',i); 
          figure('Name','Error')
          plot(t,e(:,i))
          title({sprintf('Error%d',i),sprintf(...
               'Initial Conditions = [ 0 , 0, %.2f , %.2f ]',init(3),init(4))})
          saveas(gca, fullfile(fpath, fname), 'png');
          figure('Name','State&Desired')
          fname=sprintf('2A_State%dPlus.png',i);
          plot(t,y(:,i))
          hold on
          plot(t,d(:,i))
          title({sprintf('State%d*',i),sprintf(...
               'Initial Conditions = [ 0 , 0, %.2f , %.2f ]',init(3),init(4))})
          legend('actual','desired')
          saveas(gca, fullfile(fpath, fname), 'png');
          hold off

     end
     
     %time=time(1:120); Control=Control(1:120);
     fname=sprintf('2A_Control.png');
     figure('Name','Control')
     plot(time(2:end),Control)
     title({sprintf('Control'),sprintf(...
          'Initial Conditions = [ 0 , 0, %.2f , %.2f ]',init(3),init(4))})
       
     saveas(gca, fullfile(fpath, fname), 'png');
