
 global c;
 c=1;

global m; global r; global k; global I; global J; global mi; global g;
global m_hat; global r_hat; global k_hat; global I_hat; global J_hat; global mi_hat;
I=5.6;m=20;r=0.25;k=1900;J=6.18;mi=1; g=9.8;
m_hat=20; k_hat=2000;J_hat=6;I_hat=5;r_hat=0.25;mi_hat=1.25;

global w1; global w2; global w3; global w4; global w5; global w6; global w7; global w8;

m_min=19; m_max=24; 
k_min=1500;k_max=2500;
J_min=5; J_max=10; 
I_min=4; I_max=10;
r_min=0.2; r_max=0.5;
mi_min=0.5; mi_max=3; 

%% Max Values of Uncertainty Terms.
mrj_k_max=m_max*r_max*J_max/k_min;
mrj_k_hat=m_hat*r_hat*J_hat/k_hat;
mrj_k_min=m_min*r_min*J_min/k_max;
mrj_i_max=m_max*r_max*J_max/I_min;
mrj_i_hat=m_hat*r_hat*J_hat/I_hat;
mrj_i_min=m_min*r_min*J_min/I_max;
m2r2j_ik_max=m_max^2*r_max^2*J_max/(k_min*I_min);
m2r2j_ik_hat=m_hat^2*r_hat^2*J_hat/(k_hat*I_hat);
m2r2j_ik_min=m_min^2*r_min^2*J_min/(k_max*I_max);
kj_i_max=k_max*J_max/I_min;
kj_i_hat=k_hat*J_hat/I_hat;
kj_i_min=k_min*J_min/I_max;
ij_k_max=I_max*J_max/k_min;
ij_k_hat=I_hat*J_hat/k_hat;
ij_k_min=I_min*J_min/k_max;
w1=max([mrj_k_hat-mrj_k_min,mrj_k_max-mrj_k_hat]);
w2=max([mrj_i_hat-mrj_i_min,mrj_i_max-mrj_i_hat]);
w3=max([m2r2j_ik_hat-m2r2j_ik_min,m2r2j_ik_max-m2r2j_ik_hat]);
w4=max([kj_i_hat-kj_i_min,kj_i_max-kj_i_hat]);
w5=max([k_hat-k_min,k_max-k_hat]);
w6=max([mi_hat-mi_min,mi_max-mi_hat]);
w7=max([ij_k_hat-ij_k_min,ij_k_max-ij_k_hat]);
w8=max([J_hat-J_min,J_max-J_hat]);

init=[0;0;1;1];
     options = odeset('RelTol',1e-5,'AbsTol',1e-5);
fpath=pwd;
counter=1;
%    The following script contains the code for 1st level sensitivity analysis
%    Sensitivity1
%    The following script contains the code for 2nd level sensitivity analysis
%    Sensitivity2
global epsilon, global l; global Control; global time;
     Control=[];time=-1;
     epsilon=5e-1; l=11;
     [t1,y1] = ode23s(@(t,y) ode_2_C(t,y),[0 20],init ,options);
     y1=y1'; t1=t1';
     d1=[0.1+0.5*sin(t1);0.5*cos(t1);-0.5*sin(t1);-0.5*cos(t1)];
     for tmp=1:4
     figure()
     plot(t1,y1(tmp,:))
     xlabel('Time')
     ti=sprintf('R%dS%d',counter,tmp);
     title(sprintf('State %d',tmp));
     fname=sprintf('%s.png',ti);
     legend('$\epsilon=5e-1, \lambda=11$','Interpreter','latex')
     saveas(gca, fullfile(fpath, fname), 'png');
     end
     for tmp=1:4
     figure()
     plot(t1,y1(tmp,:)-d1(tmp,:))
     xlabel('Time')
     ti=sprintf('R%dE%d',counter,tmp);
     title(sprintf('Error %d',tmp));
     fname=sprintf('%s.png',ti);
          legend('$\epsilon=5e-1, \lambda=11$','Interpreter','latex')

     saveas(gca, fullfile(fpath, fname), 'png');
     end
     
figure()
     plot(time(2:4:end),Control(1:4:end))
     xlabel('Time')
     ti=sprintf('R%dU',counter);
     title(sprintf('Control Signal'));
     fname=sprintf('%s.png',ti);
     legend('$\epsilon=5e-1, \lambda=11$','Interpreter','latex')
     saveas(gca, fullfile(fpath, fname), 'png');
     counter=counter+1;
     close all;
     
     Control=[];time=-1;
     epsilon=1;l=11;
     [t2,y2] = ode23s(@(t,y) ode_2_C(t,y),[0 20],init ,options);
     y2=y2'; t2=t2';
     d2=[0.1+0.5*sin(t2);0.5*cos(t2);-0.5*sin(t2);-0.5*cos(t2)];
     for tmp=1:4
     figure()
     plot(t2,y2(tmp,:))
     xlabel('Time')
     ti=sprintf('R%dS%d',counter,tmp);
     title(sprintf('State %d',tmp));
     legend('$\epsilon=1, \lambda=11$','Interpreter','latex')
     fname=sprintf('%s.png',ti);
     saveas(gca, fullfile(fpath, fname), 'png');
     end
     for tmp=1:4
     figure()
     plot(t2,y2(tmp,:)-d2(tmp,:))
     xlabel('Time')
     ti=sprintf('R%dE%d',counter,tmp);
     title(sprintf('Error %d',tmp));
      legend('$\epsilon=1, \lambda=11$','Interpreter','latex')
    fname=sprintf('%s.png',ti);
     saveas(gca, fullfile(fpath, fname), 'png');
     end
     figure()
     plot(time(2:4:end),Control(1:4:end))
     xlabel('Time')
     ti=sprintf('R%dU',counter);
     title(sprintf('Control Signal'));
      legend('$\epsilon=1, \lambda=11$','Interpreter','latex')
    fname=sprintf('%s.png',ti);
     saveas(gca, fullfile(fpath, fname), 'png');
          counter=counter+1;

close all;
     
     Control=[];time=-1;
     epsilon=2;l=11;
     [t3,y3] = ode23s(@(t,y) ode_2_C(t,y),[0 20],init ,options);
     y3=y3'; t3=t3';
     d3=[0.1+0.5*sin(t3);0.5*cos(t3);-0.5*sin(t3);-0.5*cos(t3)];
     for tmp=1:4
     figure()
     plot(t3,y3(tmp,:))
     xlabel('Time')
     ti=sprintf('R%dS%d',counter,tmp);
     title(sprintf('State %d',tmp));
     legend('$\epsilon=2, \lambda=11$','Interpreter','latex')
     fname=sprintf('%s.png',ti);
     saveas(gca, fullfile(fpath, fname), 'png');
     end
     for tmp=1:4
     figure()
     plot(t3,y3(tmp,:)-d3(tmp,:))
     xlabel('Time')
     ti=sprintf('R%dE%d',counter,tmp);
     title(sprintf('Error %d',tmp));
      legend('$\epsilon=2, \lambda=11$','Interpreter','latex')
    fname=sprintf('%s.png',ti);
     saveas(gca, fullfile(fpath, fname), 'png');
     end
     figure()
     plot(time(2:4:end),Control(1:4:end))
     xlabel('Time')
     ti=sprintf('R%dU',counter);
     title(sprintf('Control Signal'));
      legend('$\epsilon=2, \lambda=11$','Interpreter','latex')
    fname=sprintf('%s.png',ti);
     saveas(gca, fullfile(fpath, fname), 'png');
          counter=counter+1;

close all;

     
     %Create the total timespan disregarding the same values (e.g. 0)
tspan=sort([t1,t2,t3]);
delete=[];
for i=1:(length(tspan)-1)
     if tspan(i)==tspan(i+1)
          delete=[delete i+1];
     end
end
tspan(delete)=[];
e1=y1(1,:)-d1(1,:);
e1=interp1(t1,e1,tspan,'spline');
e2=y2(1,:)-d2(1,:);
e2=interp1(t2,e2,tspan,'spline');
    e3=y3(1,:)-d3(1,:);
e3=interp1(t3,e3,tspan,'spline');
     figure()
     plot(tspan(1:5:end),e1(1:5:end),'DisplayName','e=5e-1')
     hold on
     plot(tspan(1:5:end),e2(1:5:end),'DisplayName','e=1')

     plot(tspan(1:5:end),e3(1:5:end),'DisplayName','e=2')
     
     xlabel('Time')
     ti=sprintf('R%dErrors',counter);
     title(sprintf('ErrorsAll'));
     fname=sprintf('%s.png',ti);
        legend({'5e-1','1','2'})
        saveas(gca, fullfile(fpath, fname), 'png');
          counter=counter+1;
