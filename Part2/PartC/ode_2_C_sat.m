function [dx] = ode_2_C_sat(t,x, u_max)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
global Control; global time; global c; global l; global epsilon; 
global m; global r; global k; global I; global J; global mi; global g;
global m_hat; global r_hat; global k_hat; global I_hat; global J_hat; global mi_hat;
global q1; global q1d; global q1dd; global q1ddd; global q1dddd;
q1=0.1+0.5*sin(t); q1d=0.5*cos(t); q1dd=-0.5*sin(t); q1ddd=-0.5*cos(t); q1dddd=0.5*sin(t);
global w1; global w2; global w3; global w4; global w5; global w6; global w7; global w8;

     term_1=J_hat*I_hat/k_hat*((q1dddd+3*l*q1ddd+3*l^2*q1dd+l^3*q1d-l^3*x(2)) ...
     -3*l*(m_hat*g*r_hat*J_hat/(k_hat))*sin(x(1))*x(2)...
     +3*l*J_hat*(x(2)-x(4))...
     +3*l^2*(m_hat*g*r_hat*J_hat/(k_hat))*cos(x(1))...
     -3*l^2*J_hat*(x(3)-x(1)));

     term_2=-(m_hat*g*r_hat*J_hat/(I_hat*k_hat))*cos(x(1))*x(2)^2 ...
     +(m_hat*g*r_hat*J_hat/I_hat^2)*(sin(x(1))*(x(1)-x(3))-cos(x(1)))...
     +(m_hat^2*g^2*r_hat^2*J_hat/(I_hat^2*k_hat))*cos(x(1))*sin(x(1))...
     -(J_hat*k_hat/I_hat^2+J_hat/I_hat)*(x(1)-x(3))...
     +mi_hat/I_hat*x(4);
     
     u_eq=term_1+term_2;
     rho=w1*g*(abs(cos(x(1)))*abs(x(2))^2+3*l*abs(sin(x(1)))*abs(x(2))-3*l^2*abs(cos(x(1)))) ...
     +w2*g*(abs(sin(x(1)))*abs((x(1)-x(3)))+abs(cos(x(1))))...
     +w3*g^2*abs(sin(x(1)))*abs(cos(x(1)))...
     +(w4+w5)*abs(x(1)-x(3))+w6*abs(x(4))...
     +w7*abs(-q1dddd-3*l*q1ddd-3*l^2*q1dd-l^3*q1d+l^3*x(2))...
     +w8*abs(-3*l^2*abs(x(1)-x(3))-3*l*abs(x(2)-x(4)))+ c;


     s=m_hat*g*r_hat/I_hat*sin(x(1))*x(2)-k_hat/I_hat*(x(2)-x(4))-(-0.5*cos(t)) ...
     +3*l*(-m_hat*g*r_hat/I_hat*cos(x(1))+k_hat/I_hat*(x(3)-x(1)))-3*l*(-0.5*sin(t)) ...
     +3*l^2*(x(2)-0.5*cos(t))+l^3*(x(1)-0.1-0.5*sin(t));

     if abs(s)>=epsilon
          g1=s/abs(s);
     else
          g1=s/epsilon;
     end
     u=u_eq-rho*g1;
     if abs(u)>u_max
          u=u*(u_max/abs(u));
     end
     
     dx(1)=x(2);
     dx(2)=-m*g*r/I*cos(x(1))-k/I*(x(1)-x(3));
     dx(3)=x(4);
     dx(4)=k/J*(x(1)-x(3))-mi/J*x(4)+1/J*u;
     dx=dx';
     
     if t>time(end)
          time=[time t];
          Control=[Control u];
     end
     if any(isnan(dx))
          return;
     end
end
