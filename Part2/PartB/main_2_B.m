%% Parameters
%We store the values of the parameters as given in the problem statement
%with the following order: [real_value, min_value, max_value, mean value]:
global I; global J; global k; global m; global g; global r; global mi;
I=5.6; I_min=4; I_max=10; I_hat=7;
J=6.18; J_min=5; J_max=10; J_hat=7.5;
k=1900; k_min=1500; k_max=2500; k_hat=2200;
m=20; m_min=19; m_max=24; m_hat=21.5;
r=0.25; r_min=0.2; r_max=0.5; r_hat=0.35;
mi=1; mi_min=0.5; mi_max=3; mi_hat=1.75;
g=9.8;
%% New-Parameters
%We define some new parameters in order to simplify the calculations. Same 
%pattern in storing as before, but with an extra element. Given that the 
%first element is the actual value of the parameter, we can find the worst
%parameter estimation of it, which will be one of the boundaries of the parameter.
%Either its min or its max value. So, the fifth element contains the worst
%estimation of each parameter we might do. (notice that it isn't possible that 
%4 and 5 hold together in the same time)
%The formula of each parameter is given as well:
global a1; global a2; global a3; global a4; global a5;
global a1_hat; global a2_hat; global a3_hat; global a4_hat; global a5_hat;
a1=m*g*r/I; a1_hat=m_hat*g*r_hat/I_hat;
a2=k/I; a2_hat=k_hat/I_hat;
a3=k/J; a3_hat=k_hat/J_hat;
a4=mi/J; a4_hat=mi_hat/J_hat;
a5=1/J; a5_hat=1/J_hat;
init=[0;0;0;0];
%init=[0;0;2*pi;2*pi]
%init=[0;0;pi;-pi];
global C;global time;
time=-1; C=[];
%% ODE
options=odeset('RelTol',1e-6,'AbsTol',1e-6);
%t,y are column vectors
[t,y] = ode45(@(t,y) ode_2_B(t,y),[0 20],init,options);
%In order to have a closer look:
%t=t(280:end);y=y(280:end,:);
%Error Formulation
d=[0.1+0.5*sin(t),0.5*cos(t),-0.5*sin(t),-0.5*cos(t)];
e=y-d;

%% Plots
fpath='C:\Users\ifout\Desktop\plots\b';
for i=1:4
     fname=sprintf('2A_State%d.png',i);
     figure('Name','State')
     plot(t,y(:,i))
     title({sprintf('State%d',i),sprintf(...
          'Initial Conditions = [ 0 , 0, %.2f , %.2f ]',init(3),init(4))})
       
     saveas(gca, fullfile(fpath, fname), 'png');
     fname=sprintf('2A_Error%d.png',i); 
     figure('Name','Error')
     plot(t,e(:,i))
     title({sprintf('Error%d',i),sprintf(...
          'Initial Conditions = [ 0 , 0, %.2f , %.2f ]',init(3),init(4))})
     saveas(gca, fullfile(fpath, fname), 'png');
     figure('Name','State&Desired')
     fname=sprintf('2A_State%dPlus.png',i);
     plot(t,y(:,i))
     hold on
     plot(t,d(:,i))
     title({sprintf('State%d*',i),sprintf(...
          'Initial Conditions = [ 0 , 0, %.2f , %.2f ]',init(3),init(4))})
     legend('actual','desired')
     saveas(gca, fullfile(fpath, fname), 'png');
     hold off
     
end

     %closer look
     %time=time(360:end); C=C(360:end);
     fname=sprintf('2A_Control.png');
     figure('Name','Control')
     plot(time(2:end),C)
     title({sprintf('Control'),sprintf(...
          'Initial Conditions = [ 0 , 0, %.2f , %.2f ]',init(3),init(4))})
       
     saveas(gca, fullfile(fpath, fname), 'png');
