function [dx] = ode_2_B(t,x)

%% Summary
%    ode_robotarm_b: models the D.E. of the system (with uncertainties).
%    Input: current state and time.
%    Output: the derivative of the state.
%    This function is itself an input to ode45 in order
%    to model the system with respect to time.
%    It is being called in the robotarm_b script file.

%% Global Parameters
     global a1; global a2; global a3; global a4; global a5;
     global C; global time; 
     global a1_hat; global a2_hat; global a3_hat; global a4_hat; global a5_hat;

%% State-Space vector Transformation
     %The desired input signal v is expressed in terms of z,
     %the transformed state space vector. In order to keep
     %the system modelled in the original state, we need to 
     %insert the transformation from x to z, for v.
     z1=x(1);
     z2=x(2);
     z3=-a1(1)*cos(x(1))-a2(1)*(x(1)-x(3));
     z4=a1(1)*sin(x(1))*x(2)-a2(1)*x(2)+a2(1)*x(4);
     
%% Transformed System (z)
     %From the transformed system, only the 4th D.E. is of our
     %interest. The polynomial of this D.E. is implemented below
     %in terms of the original vector, x. It is used to design
     %the controller. Apart from it, we use the original state.
%      p=a1(1)*cos(x(1))*x(2)^2 + (a1(1)*sin(x(1))- ...
%           a2(1))*(-a1(1)*cos(x(1))-a2(1)*(x(1)-x(3))) ...
%           + a2(1)*a3(1)*(x(1)-x(3))-a2(1)*a4(1)*x(4);
%      
%% Desired input signal
     v=0.5*sin(t)-40*(z4+0.5*cos(t))-600*(z3+0.5*sin(t)) ...
          -4000*(z2-0.5*cos(t))-10000*(z1-0.1-0.5*sin(t));
     
%% Transformed System (z) Estimation     
     %The polynomila of the 4th Equation is used in the Control Law.
     %We need to insert the uncertainties in the controller, because
     %while designing it, we do not know the exact parameters' values.
     %So, simply, instead of the actual values, we use the estimated ones.
     %For that, we saved two different approximations during the setup.
     %The firs one is a good approximation and the second one a worse one.
     %For more details check "setup.m" script file.
     
     p_hat=a1_hat*cos(x(1))*x(2)^2 + (a1_hat*sin(x(1))- ...
          a2_hat)*(-a1_hat*cos(x(1))-a2_hat*(x(1)-x(3))) ...
          + a2_hat*a3_hat*(x(1)-x(3))-a2_hat*a4_hat*x(4);

%% Actual input signal
     u=-p_hat/(a2_hat*a5_hat)+v/(a2_hat*a5_hat);
     %Storing input signal even though it is not a state
     if t>time(end)
          C=[C u];
          time=[time t];
     end
   
%% Original System (x)
     dx(1)=x(2);
     dx(2)=-a1(1)*cos(x(1))-a2(1)*(x(1)-x(3));
     dx(3)=x(4);
     dx(4)=a3(1)*(x(1)-x(3))-a4(1)*x(4)+a5(1)*u;
     dx=dx';

end

%% Transformed System Implementation
%      global a; global b; global c; global d; global e;
%      global a_hat; global b_hat; global c_hat; global d_hat; global e_hat;
%      
%      p=a*cos(z(1))*(-2*a*sin(z(1))+2*b-c+z(2)^2) ...
%           -a*sin(z(1))*(z(3)-d*z(2)) ...
%           +z(3)*(b-c)-b*d*z(2)-d*z(4);
%      p_hat=a_hat*cos(z(1))*(-2*a_hat*sin(z(1))+2*b_hat-c_hat+ ...
%           z(2)^2) -a_hat*sin(z(1))*(z(3)-d_hat*z(2)) ...
%           +z(3)*(b_hat-c_hat)-b_hat*d_hat*z(2)-d_hat*z(4);
%      v=0.5*sin(t)-40*(z(4)+0.5*cos(t))-600*(z(3)+0.5*sin(t)) ...
%           -4000*(z(2)-0.5*cos(t))-10000*(z(1)-0.1-0.5*sin(t));
%      dz(1)=z(2);
%      dz(2)=z(3);
%      dz(3)=z(4);
%      dz(4)=p+b*e*v/(b_hat*e_hat)-b*e*p_hat/(b_hat*e_hat);
%      dz=dz';
