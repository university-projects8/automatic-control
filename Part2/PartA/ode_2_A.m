function [dx] = ode_robotarm_a(t,x)

%% Summary
%    ode_robotarm_a: models the D.E. of the system.
%    Input: current state and time.
%    Output: the derivative of the state.
%    This function is itself an input to ode45 in order
%    to model the system with respect to time.
%    It is being called in the robotarm_a script file.

%% Global Parameters
     global a1; global a2; global a3; global a4; global a5;
     global C; global time; 

%% State-Space vector Transformation
     %The desired input signal v is expressed in terms of z,
     %the transformed state space vector. In order to keep
     %the system modelled in the original state, we need to 
     %insert the transformation from x to z, for v.
     z1=x(1);
     z2=x(2);
     z3=-a1(1)*cos(x(1))-a2(1)*(x(1)-x(3));
     z4=a1(1)*sin(x(1))*x(2)-a2(1)*x(2)+a2(1)*x(4);
     
%% Transformed System (z)
     %From the transformed system, only the 4th D.E. is of our
     %interest. The polynomial of this D.E. is implemented below
     %in terms of the original vector, x. It is used to design
     %the controller. Apart from it, we use the original state.
     p=a1(1)*cos(x(1))*x(2)^2 + (a1(1)*sin(x(1))- ...
          a2(1))*(-a1(1)*cos(x(1))-a2(1)*(x(1)-x(3))) ...
          + a2(1)*a3(1)*(x(1)-x(3))-a2(1)*a4(1)*x(4);
     
%% Desired input signal
     v=0.5*sin(t)-40*(z4+0.5*cos(t))-600*(z3+0.5*sin(t)) ...
          -4000*(z2-0.5*cos(t))-10000*(z1-0.1-0.5*sin(t));
     
%% Actual input signal
     u=-p/(a2(1)*a5(1))+v/(a2(1)*a5(1));
     %Storing input signal even though it is not a state
     if t>time(end)
          C=[C u];
          time=[time t];
     end
     
%% Original System (x)
     dx(1)=x(2);
     dx(2)=-a1(1)*cos(x(1))-a2(1)*(x(1)-x(3));
     dx(3)=x(4);
     dx(4)=a3(1)*(x(1)-x(3))-a4(1)*x(4)+a5(1)*u;
     dx=dx';

end
